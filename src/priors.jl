using NCDatasets
using Distributions

"""
Read prior information from SWORD from `ncfile` NetCDF file.
Requires prior information on the minimum observed water surface elevation (`minH`).
"""
function read_sword_data(ncfile::String, minH::Float64, name="reach")
    ds = Dataset(ncfile)
    g = NCDatasets.group(ds, name)
    Qm = g["Qhat"][:]
    Qc = g["Qsd"][:] / Qm
    Qub = exp(g["upperbound_logQ"][:])
    Qlb = exp(g["lowerbound_logQ"][:])
    Qₚ = Truncated(LogNormal(log(Qm/sqrt(Qc^2)), log(1+Qc^2)), Qlb, Qub)
    n = [exp(g[var][:]) for var in ["logn_hat", "logn_sd", "upperbound_logn", "lowerbound_logn"]]
    nₚ = Truncated(Normal(n[1], n[2]), n[4], n[3])
    r = [exp(g[var][:]) for var in ["logr_hat", "logr_sd", "upperbound_logr", "lowerbound_logr"]]
    rₚ = Truncated(Normal(r[1], r[2]), r[4], r[3])
    z = [exp(g[var][:]) for var in ["logDb_hat", "logDb_sd", "upperbound_logDb", "lowerbound_logDb"]]
    zₚ = Truncated(Normal(minH-z[1], z[2]), minH-z[3], minH-z[4])
    Qₚ, nₚ, rₚ, zₚ
end

"""
Derive prior probability distributions from database.
"""
function priors(ncfile::String, H::Array{Float64, 2}; name="reach")
    minH = minimum(H[1, :])
    Qₚ, nₚ, rₚ, zₚ = read_sword_data(ncfile, minH, name)
    Qₚ, nₚ, rₚ, zₚ
end